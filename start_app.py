import json
import time

import peewee
from flask import (Flask, render_template, jsonify, request)
from flask_expects_json import expects_json

import json_schemas
from Configuration import Configuration
from Database import Database, Customer, Car, RentalBook, CustomerDetails

app = Flask(__name__, template_folder="templates")
c = Configuration()


@app.route('/')
def home():
    """
    Home page available on host:port/ indicates that application is up and running
    :return: home.html
    """
    return render_template('home.html'), 200


########################################################################
# REST API controller
########################################################################
@app.route('/api/test')
def api_test():
    """
    REST API method that is for testing purposes only
    :return: json response
    """
    return jsonify(dict(status='running')), 200


@app.route('/api/customer/add_customer', methods=['POST'])
@expects_json(json_schemas.add_user_schema)
def api_add_customer():
    """
    REST API method that will add customer into the database
    :return: json response
    """
    data: json = request.json

    customer_name: str = data.get('customer_name')
    customer_surname: str = data.get('customer_surname')
    customer_email: str = data.get('customer_email')

    try:
        customer = Customer.select().where(Customer.customer_email == customer_email)
        #
        if customer.exists():
            message: str = 'Customer already exist in the database.'
        else:
            Customer.create(customer_name=customer_name,
                            customer_surname=customer_surname,
                            customer_email=customer_email)
            message: str = 'Customer has been inserted to the database.'
        #
        customer = Customer.select().where(Customer.customer_email == customer_email).get()
    except (peewee.IntegrityError, peewee.ProgrammingError):
        return jsonify(dict(message='Unknown technical ERROR.')), 409

    # Return
    response = app.response_class(
        response=json.dumps(dict(customer_id=f'{customer.customer_id}', message=message)),
        status=201,
        mimetype='application/json'
    )
    return response


@app.route('/api/customer/delete_customer/email/', defaults={'email': None}, methods=['DELETE'])
@app.route('/api/customer/delete_customer/email/<email>', methods=['DELETE'])
def api_delete_customer_by_email(email: str):
    """
    REST API method that will delete particular customer by email
    :param email: Optional parameter
    :return: json response
    """
    if email is not None:
        Customer.delete().where(Customer.customer_email == email).execute()

    # Return
    response = app.response_class(
        status=204,
        mimetype='application/json'
    )
    return response


@app.route('/api/customer/delete_customer/id/', defaults={'customer_id': None}, methods=['DELETE'])
@app.route('/api/customer/delete_customer/id/<customer_id>', methods=['DELETE'])
def api_delete_customer_by_id(customer_id: int):
    """
    REST API method that will delete particular customer  by id
    :param customer_id: Optional parameter
    :return: json response
    """
    if customer_id is not None:
        Customer.delete_by_id(customer_id)

    # Return
    response = app.response_class(
        status=204,
        mimetype='application/json'
    )
    return response


@app.route('/api/customer/select_customer/', defaults={'email': None}, methods=['GET'])
@app.route('/api/customer/select_customer/<email>', methods=['GET'])
def api_select_customer(email: str):
    """
    REST API method that will return particular customer if email is provided or list of all customers if email is not provided
    :param email: Optional parameter
    :return: json response
    """
    if email is not None:
        customer_response = Customer.select().where(Customer.customer_email == email)
    else:
        customer_response = Customer.select()
    #
    data: list = [i.serialize for i in customer_response]

    # Return
    response = app.response_class(
        response=json.dumps(data),
        status=200,
        mimetype='application/json'
    )
    return response


@app.route('/api/car/add_car', methods=['POST'])
@expects_json(json_schemas.add_car_schema)
def api_add_car():
    """
    REST API method that will add car into the database
    :return: json response
    """
    data: json = request.json

    car_name: str = data.get('car_name')
    car_type: str = data.get('car_type')

    try:
        Car.create(car_name=car_name, car_type=car_type)
        car = Car.select().order_by(Car.car_id.desc()).get()
        message: str = 'Car has been inserted to the database.'
    except (peewee.IntegrityError, peewee.ProgrammingError):
        return jsonify(dict(message='Unknown technical ERROR.')), 409

    # Return
    response = app.response_class(
        response=json.dumps(dict(car_id=f'{car.car_id}', message=message)),
        status=201,
        mimetype='application/json'
    )
    return response


@app.route('/api/car/select_car/', defaults={'car_id': None}, methods=['GET'])
@app.route('/api/car/select_car/<car_id>', methods=['GET'])
def api_select_car(car_id: int):
    """
    REST API method that will return particular car if ID is provide or list of all cars if ID is not provided
    :param car_id: Optional parameter
    :return: json response
    """
    if car_id is not None:
        car_response = Car.select().where(Car.car_id == car_id)
    else:
        car_response = Car.select()
    #
    data: list = [i.serialize for i in car_response]

    # Return
    response = app.response_class(
        response=json.dumps(data),
        status=200,
        mimetype='application/json'
    )
    return response


@app.route('/api/car/delete_car/id/', defaults={'car_id': None}, methods=['DELETE'])
@app.route('/api/car/delete_car/id/<car_id>', methods=['DELETE'])
def api_delete_car_by_id(car_id: int):
    """
    REST API method that will delete particular car by id
    :param car_id: Optional parameter
    :return: json response
    """
    if car_id is not None:
        Car.delete_by_id(car_id)

        # Return
    response = app.response_class(
        status=204,
        mimetype='application/json'
    )
    return response


@app.route('/api/booking/book_car/<car_id>/renter/<customer_id>', methods=['POST'])
@expects_json(json_schemas.book_car_schema)
def api_book_car(car_id: int, customer_id: int):
    """
    REST API method that will book up a particular car for a particular customer
    :param car_id: Mandatory parameter
    :param customer_id: Mandatory parameter
    :return: json response
    """
    data: json = request.json
    #
    rented_from: str = data.get('rented_from')
    rented_to: str = data.get('rented_to')

    # validate input
    if not validate_date(date=rented_from):
        return jsonify(dict(message='rented_from is not valid date YYYY-MM-DD')), 409

    if not validate_date(date=rented_to):
        return jsonify(dict(message='rented_to is not valid date YYYY-MM-DD')), 409

    # Check if car is available in requested date range
    is_car_booked: int = RentalBook.select().join(Car).where((RentalBook.car == car_id) & (RentalBook.rented_from >= rented_from) & (RentalBook.rented_to <= rented_to)).count()

    if is_car_booked > 0:
        booked_car_details = Car.get_by_id(car_id)
        # TODO return booking ID
        return jsonify(dict(message=f'Car <{booked_car_details.car_id}:{booked_car_details.car_name}:{booked_car_details.car_type}> is already booked in such range <{rented_from}:{rented_to}>')), 409
    else:
        try:
            RentalBook.create(customer=Customer.get_by_id(customer_id),
                              car=Car.get_by_id(car_id),
                              rented_from=rented_from,
                              rented_to=rented_to)
            booking = RentalBook.select().order_by(RentalBook.booking_id.desc()).get()
            message: str = 'Booking has been inserted to the database.'
        except Customer.DoesNotExist:
            return jsonify(dict(message=f'Customer id: {customer_id} doesnt exist in the database.')), 404
        except Car.DoesNotExist:
            return jsonify(dict(message=f'Car id: {car_id} doesnt exist in the database.')), 404
        except (peewee.IntegrityError, peewee.ProgrammingError):
            return jsonify(dict(message='Unknown technical ERROR.')), 409

    # Return
    response = app.response_class(
        response=json.dumps(dict(booking_id=f'{booking.booking_id}', message=message)),
        status=201,
        mimetype='application/json'
    )
    return response


@app.route('/api/booking/select_booking/', defaults={'booking_id': None}, methods=['GET'])
@app.route('/api/booking/select_booking/<booking_id>', methods=['GET'])
def api_select_booking(booking_id: int):
    """
    REST API method that will return particular booking if booking_id is provided or list of all bookings if booking_id is not provided
    :param booking_id: Optional parameter
    :return: json response
    """
    if booking_id is not None:
        booking_response = RentalBook.select().where(RentalBook.booking_id == booking_id)
    else:
        booking_response = RentalBook.select()
    #
    data: list = [i.serialize for i in booking_response]

    # Return
    response = app.response_class(
        response=json.dumps(data),
        status=200,
        mimetype='application/json'
    )
    return response


@app.route('/api/booking/delete_booking/car/<car_id>', methods=['DELETE'])
@expects_json(json_schemas.book_car_schema)
def api_delete_booking_by_car(car_id: int):
    data: json = request.json
    #
    rented_from: str = data.get('rented_from')
    rented_to: str = data.get('rented_to')

    # Check if car is available in requested date range
    is_car_booked: int = RentalBook.select().join(Car).where((RentalBook.car == car_id) & (RentalBook.rented_from >= rented_from) & (RentalBook.rented_to <= rented_to)).count()

    if is_car_booked > 0:
        booking_details = RentalBook.select().join(Car).where((RentalBook.car == car_id) & (RentalBook.rented_from >= rented_from) & (RentalBook.rented_to <= rented_to))
        booking_id = booking_details[0]
        RentalBook.delete().where(RentalBook.booking_id == booking_id).execute()

    # Return
    response = app.response_class(
        status=204,
        mimetype='application/json'
    )
    return response


@app.route('/api/booking/delete_booking/booking/<booking_id>', methods=['DELETE'])
def api_delete_booking_by_booking(booking_id: int):
    RentalBook.delete().where(RentalBook.booking_id == booking_id).execute()

    # Return
    response = app.response_class(
        status=204,
        mimetype='application/json'
    )
    return response


@app.route('/api/customer/add_details/<customer_id>', methods=['POST'])
@expects_json(json_schemas.add_user_details_schema)
def api_add_customer_details(customer_id: int):
    data: json = request.json
    #
    customer_address: str = data.get('customer_address')
    customer_city: str = data.get('customer_city')
    customer_country: str = data.get('customer_country')
    customer_phone: str = data.get('customer_phone')

    try:
        customer_details = CustomerDetails.select().where(CustomerDetails.customer == customer_id)
        #
        if customer_details.exists():
            message: str = 'Customer details already exist in the database.'
        else:
            CustomerDetails.create(customer=Customer.get_by_id(customer_id),
                                   customer_address=customer_address,
                                   customer_city=customer_city,
                                   customer_country=customer_country,
                                   customer_phone=customer_phone)
            message: str = 'Customer details has been inserted to the database.'
        #
        customer_details = CustomerDetails.select().where(CustomerDetails.customer == customer_id).get()
    except Customer.DoesNotExist:
        return jsonify(dict(message=f'Customer id: {customer_id} doesnt exist in the database.')), 404
    except (peewee.IntegrityError, peewee.ProgrammingError):
        return jsonify(dict(message='Unknown technical ERROR.')), 409

    # Return
    response = app.response_class(
        response=json.dumps(dict(customer_details_id=f'{customer_details.customer_info_id}', message=message)),
        status=201,
        mimetype='application/json'
    )
    return response


@app.route('/api/customer/update_details/<customer_id>', methods=['PUT'])
@expects_json(json_schemas.update_user_details_schema)
def api_update_customer_details(customer_id: int):
    data: json = request.json
    #
    customer_address: str = data.get('customer_address')
    customer_city: str = data.get('customer_city')
    customer_country: str = data.get('customer_country')
    customer_phone: str = data.get('customer_phone')

    try:
        customer_details = CustomerDetails.select().where(CustomerDetails.customer == customer_id)
        #
        if customer_details.exists():
            CustomerDetails.update(customer_address=customer_address,
                                   customer_city=customer_city,
                                   customer_country=customer_country,
                                   customer_phone=customer_phone).where(CustomerDetails.customer == customer_id).execute()
            message: str = 'Customer details already exist in the database. Details were updated.'
        else:
            CustomerDetails.insert(customer=Customer.get_by_id(customer_id),
                                   customer_address=customer_address,
                                   customer_city=customer_city,
                                   customer_country=customer_country,
                                   customer_phone=customer_phone).execute()
            message: str = 'Customer details has been inserted to the database.'
        #
        customer_details = CustomerDetails.select().where(CustomerDetails.customer == customer_id).get()
    except (peewee.IntegrityError, peewee.ProgrammingError):
        if issubclass(Customer.DoesNotExist, Exception):
            return jsonify(dict(message=f'Customer id: {customer_id} doesnt exist in the database.')), 404
        else:
            return jsonify(dict(message='Unknown technical ERROR.')), 409

    # Return
    response = app.response_class(
        response=json.dumps(dict(customer_details_id=f'{customer_details.customer_info_id}', message=message)),
        status=201,
        mimetype='application/json'
    )
    return response


def validate_date(date: str) -> bool:
    try:
        time.strptime(date, '%Y-%m-%d')
        return True
    except ValueError:
        return False


# If we're running in stand alone mode, run the application
if __name__ == '__main__':
    # Initialize schema
    Database().create_database_model()
    Database().connect_database()
    app.run(host=c.getServerIpAddress(), port=c.getServerPort(), debug=False)
