add_user_schema = {
    'type': 'object',
    'properties': {
        'customer_name': {'type': 'string'},
        'customer_surname': {'type': 'string'},
        'customer_email': {'type': 'string'}
    },
    'required': ['customer_email']
}

add_user_details_schema = {
    'type': 'object',
    'properties': {
        'customer_address': {'type': 'string'},
        'customer_city': {'type': 'string'},
        'customer_country': {'type': 'string'},
        'customer_phone': {'type': 'string'}
    },
    'required': []
}

update_user_details_schema = {
    'type': 'object',
    'properties': {
        'customer_address': {'type': 'string'},
        'customer_city': {'type': 'string'},
        'customer_country': {'type': 'string'},
        'customer_phone': {'type': 'string'}
    },
    'required': []
}

add_car_schema = {
    'type': 'object',
    'properties': {
        'car_name': {'type': 'string'},
        'car_type': {'type': 'string'}
    },
    'required': ['car_name', 'car_type']
}

book_car_schema = {
    'type': 'object',
    'properties': {
        'rented_from': {'type': 'string'},
        'rented_to': {'type': 'string'}
    },
    'required': ['rented_from', 'rented_to']
}
