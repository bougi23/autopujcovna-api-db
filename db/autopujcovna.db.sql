BEGIN TRANSACTION;
CREATE TABLE IF NOT EXISTS `customers` (
	`customer_id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	`customer_name`	TEXT NOT NULL,
	`customer_surname`	TEXT NOT NULL,
	`customer_email`	TEXT NOT NULL
);
CREATE TABLE IF NOT EXISTS `customerDetails` (
	`customer_info_id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	`customer_id`	INTEGER NOT NULL,
	`customer_address`	TEXT,
	`customer_city`	TEXT,
	`customer_country`	TEXT,
	`customer_phone`	TEXT,
	FOREIGN KEY(`customer_id`) REFERENCES `customers`(`customer_id`) ON DELETE CASCADE
);
CREATE TABLE IF NOT EXISTS `cars` (
	`car_id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	`car_name`	TEXT NOT NULL,
	`car_type`	TEXT NOT NULL
);
CREATE TABLE IF NOT EXISTS `bookings` (
	`booking_id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	`customer_id`	INTEGER NOT NULL,
	`car_id`	INTEGER NOT NULL,
	`rented_from`	DATE NOT NULL,
	`rented_to`	DATE NOT NULL,
	FOREIGN KEY(`customer_id`) REFERENCES `customers`(`customer_id`) ON DELETE CASCADE,
	FOREIGN KEY(`car_id`) REFERENCES `cars`(`car_id`) ON DELETE CASCADE
);

CREATE INDEX IF NOT EXISTS `rentalbook_customer_id` ON `bookings` (
	`customer_id`
);

CREATE INDEX IF NOT EXISTS `rentalbook_car_id` ON `bookings` (
	`car_id`
);

CREATE UNIQUE INDEX IF NOT EXISTS `customerdetails_customer_id` ON `customerDetails` (
	`customer_id`
);

CREATE UNIQUE INDEX IF NOT EXISTS `customer_customer_email` ON `customers` (
	`customer_email`
);
COMMIT;
