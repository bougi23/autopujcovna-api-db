#   Databázové systémy I - projekt autopůjčovna

Projekt se skládá z relační databáze navržené v SQLite a REST api vrsty, která umožňuje přístup k datům v databázi.


# REST API endpointy

## [POST] /api/customer/add_customer

Tento endpoint byl navržen pro přidávání zákazníků autopůjčovny do databáze.

klíč | datový typ  | popis
--- | --- | ---
customer_name  | string  | Jméno zákazníka např. Jan
customer_surname  | string  | Příjmení zákazníka např. Novák
customer_email  | string  | Emailová adresa na zákazníka např. jan@novak.cz (Emailová adresa je unikátní identifikátor)

Validní json požadavek:

    {
		"customer_name":"Jan",
		"customer_surname":"Novák",
		"customer_email":"jan@novak.cz"
	}

Validní json odpověď:

**HTTP status code:** 201 CREATED

    {
        "message": "Customer has been inserted to the database.",
	    "customer_id":  "3"
    }

nebo

**HTTP status code:** 201 CREATED

    {
        "message": "Customer already exist in the database.",
	    "customer_id":  "3"
    }


Nevalidní json odpověď:

**HTTP status code:** 409 CONFLICT

    {
        "message":  "Unknown technical ERROR."
    }



## [GET] /api/customer/select_customer

Tento endpoint zobrazí seznam všech uživatelů v databazi.

Validní json odpověď:

**HTTP status code:** 200 OK

    [
        {
            "customer_id": 1,
            "customer_name": "Jmeno",
            "customer_surname": "Prijmeni",
            "customer_email": "a@b.cz"
        },
        {
            "customer_id": 2,
            "customer_name": "Jmeno",
            "customer_surname": "Prijmeni",
            "customer_email": "a@bcd.cz"
        },
        {
            "customer_id": 3,
            "customer_name": "Jmeno",
            "customer_surname": "Prijmeni",
            "customer_email": "a@bcde.cz"
        }
    ]


## [GET] /api/customer/select_customer/[email]

Tento endpoint zobrazí konkrétní detaily uživatele se zadaným emailem z databáze.

Validní json odpověď:

**HTTP status code:** 200 OK

    [
        {
            "customer_id": 1,
            "customer_name": "Jmeno",
            "customer_surname": "Prijmeni",
            "customer_email": "a@b.cz"
        }
    ]

## [DELETE] /api/customer/delete_customer/[email]

Tento endpoint slouží ke smazání uživatele z databáze se specifickým emailem.

Validni json odpověď je prázdná odpověď.

**HTTP status code:** 204 NO CONTENT

## [POST] /api/car/add_car

Tento endpoint slouží k přidávání aut do databáze autopůjčovny.

klíč | datový typ  | popis
--- | --- | ---
car_name  | string  | Jméno automobilu např. Audi
car_type  | string  | Typ automobilu např. RS6

Validní json požadavek:

    {
	    "car_type":"Audi",
	    "car_name":"RS6"
	}

Validní json odpověď:

**HTTP status code:** 201 CREATED

    {
        "message: "Car has been inserted to the database.",
	    "car_id": "3"
    }

Nevalidní json odpověď:

**HTTP status code:** 409 CONFLICT

    {
        "message":  "Unknown technical ERROR."
    }

## [GET] /api/car/select_car

Tento endpoint vrací seznam všech aut v databázi.

Validní json odpověď:

**HTTP status code:** 200 OK

    [
        {
            "car_id": 1,
            "car_name": "AUDI",
            "car_type": "RS3"
        },
        {
            "car_id": 2,
            "car_name": "AUDI",
            "car_type": "RS4"
        },
        {
            "car_id": 3,
            "car_name": "AUDI",
            "car_type": "RS6"
        }
    ]


## [DELETE] /api/car/delete_car/id/[car_id]/

Tento endpoind smaže vytvořené auto v databazí a kaskádně všechny reference.

Validni json odpověď je prázdná odpověď.

**HTTP status code:** 204 NO CONTENT


## [POST] /api/car/book_car/[car_id]/renter/[renter_id]

Tento endpoint vytvoří registraci auta patřičnému pronajimateli v zadaných dnech.

Validní json požadavek:

Požadavek níže vytvoří registraci auto s car_id 1 pronajimateli s customer_id 1 na dobu od 27.3.2019 - 28.3.2019
/api/car/book_car/1/renter/1

	{
    	"rented_from":"2019-03-27",
	    "rented_to":"2019-03-28"
	}

Validní json odpověď:

**HTTP status code:** 201 CREATED

    {
        "message": "Booking has been inserted to the database.",
	    "booking_id":  "3"
    }


Nevalidní json odpověď:

**HTTP status code:** 409 CONFLICT

Tato odpověď je vrácena serverem v případě, že je zadán chybně formát data rented_from YYYY-MM-DD

    {
        "message":  "rented_from is not valid date YYYY-MM-DD"
    }


Nevalidní json odpověď:

**HTTP status code:** 409 CONFLICT

Tato odpověď je vrácena serverem v případě, že je zadán chybně formát data rented_to YYYY-MM-DD

    {
        "message":  "rented_to is not valid date YYYY-MM-DD"
    }


Nevalidní json odpověď:

**HTTP status code:** 409 CONFLICT

Tato odpověď je vrácena serverem v případě, že je auto v zadaném časovém intervalu již rezervováno stejným anebo jiným pronajimatelem.

    {
        "message": "Car <1:AUDI:RS3> is already booked in such range <2019-03-27:2019-03-28>"
    }

Nevalidní json odpověď:

**HTTP status code:** 409 CONFLICT

Tato odpověď je vrácena serverem v případě, že uživatel (jeho ID) v databázi neexistuje

    {
        "message":  "Customer id: 1 doesnt exist in the database."
    }

Nevalidní json odpověď:

**HTTP status code:** 409 CONFLICT

Tato odpověď je vrácena serverem v případě, že auto (jeho ID) v databázi neexistuje

    {
        "message":  "Car id: 1 doesnt exist in the database."
    }

Nevalidní json odpověď:

**HTTP status code:** 409 CONFLICT

    {
        "message":  "Unknown technical ERROR."
    }


## [GET] /api/booking/select_booking/[booking_id]

Tento endpoint vrací seznam všech registraci v databázi.

Validní json odpověď:

**HTTP status code:** 200 OK

    [
        {
            "booking_id": 4,
            "customer_id": "1",
            "car_id": "2",
            "rented_from": "2019-04-27",
            "rented_to": "2019-04-28"
        },
        {
            "booking_id": 5,
            "customer_id": "1",
            "car_id": "2",
            "rented_from": "2019-05-27",
            "rented_to": "2019-05-28"
        }
    ]


## [DELETE] /api/car/delete_booking/[booking_id]
Tento endpoint slouží ke smazání rezervace z databáze se specifickým id rezervace.

Validni json odpověď je prázná odpověď.

**HTTP status code:** 204 NO CONTENT

## [DELETE] /api/car/delete_booking/[car_id]
Tento endpoint slouží ke smazání rezervace z databáze se specifickým id auta.

Validni json odpověď je prázná odpověď.

**HTTP status code:** 204 NO CONTENT
