from peewee import *
from playhouse.sqlite_ext import AutoIncrementField

from Configuration import Configuration

c: Configuration = Configuration()

DB_NAME = 'autopujcovna'
DB_DIR_PATH = c.get_app_path() + '/db/'


class Database(object):
    database = None

    @staticmethod
    def create_database_model() -> None:
        database = SqliteDatabase(database=DB_DIR_PATH + DB_NAME + '.db', thread_safe=True)
        database.create_tables(models=[Customer, Car, RentalBook, CustomerDetails])

    @staticmethod
    def connect_database() -> None:
        database = SqliteDatabase(database=DB_DIR_PATH + DB_NAME + '.db', thread_safe=True)
        database.connect()


######################################################################################################################
# Database model schema
######################################################################################################################
class BaseModel(Model):
    class Meta:
        database = SqliteDatabase(database=DB_DIR_PATH + DB_NAME + '.db', thread_safe=True)
        database.pragma('foreign_keys', 1, permanent=True)


class Customer(BaseModel):
    # Table customers definition
    customer_id = AutoIncrementField()
    customer_name = TextField(null=False)
    customer_surname = TextField(null=False)
    customer_email = TextField(unique=True, null=False)

    @property
    def serialize(self):
        data = {
            'customer_id': self.customer_id,
            'customer_name': str(self.customer_name).strip(),
            'customer_surname': str(self.customer_surname).strip(),
            'customer_email': str(self.customer_email).strip()
        }

        return data

    def __repr__(self):
        return "{}, {}, {}, {}".format(
            self.customer_id,
            self.customer_name,
            self.customer_surname,
            self.customer_email
        )

    class Meta:
        table_name = 'customers'


class Car(BaseModel):
    # Table cars definition
    car_id = AutoIncrementField()
    car_name = TextField(null=False)
    car_type = TextField(null=False)

    @property
    def serialize(self):
        data = {
            'car_id': self.car_id,
            'car_name': str(self.car_name).strip(),
            'car_type': str(self.car_type).strip()
        }

        return data

    def __repr__(self):
        return "{}, {}, {}".format(
            self.car_id,
            self.car_name,
            self.car_type
        )

    class Meta:
        table_name = 'cars'


class RentalBook(BaseModel):
    # Table bookings definition
    booking_id = AutoIncrementField()
    customer = ForeignKeyField(Customer, column_name='customer_id', on_delete='CASCADE')
    car = ForeignKeyField(Car, column_name='car_id', on_delete='CASCADE')
    rented_from = DateField()
    rented_to = DateField()

    @property
    def serialize(self):
        data = {
            'booking_id': self.booking_id,
            'customer_id': str(self.customer).strip(),
            'car_id': str(self.car).strip(),
            'rented_from': str(self.rented_from).strip(),
            'rented_to': str(self.rented_to).strip()
        }

        return data

    def __repr__(self):
        return "{},{},{}, {}, {}".format(
            self.booking_id,
            self.customer,
            self.car,
            self.rented_from,
            self.rented_to
        )

    class Meta:
        table_name = 'bookings'


class CustomerDetails(BaseModel):
    # Table customer details definition
    customer_info_id = AutoIncrementField()
    customer = ForeignKeyField(Customer, column_name='customer_id', unique=True, on_delete='CASCADE')
    customer_address = TextField(null=True)
    customer_city = TextField(null=True)
    customer_country = TextField(null=True)
    customer_phone = TextField(null=True)

    class Meta:
        table_name = 'customerDetails'
