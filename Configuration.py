import json
import os

_d = os.path.dirname
_ROOT_FOLDER = os.path.join(_d(os.path.abspath(__file__)))


class Singleton(type):
    """
    Define an Instance operation that lets clients access its unique
    instance.
    """

    def __init__(cls, name, bases, attrs, **kwargs):
        super().__init__(name, bases, attrs)
        cls._instance = None

    def __call__(cls, *args, **kwargs):
        if cls._instance is None:
            cls._instance = super().__call__(*args, **kwargs)
        return cls._instance


class Configuration(object, metaclass=Singleton):
    def __init__(self):
        self.configFilePath = _ROOT_FOLDER + '\config\config.json'
        self.serverPort: int = None
        self.serverIpAddress: str = None

        data = None
        try:
            data = json.load(open(self.configFilePath))
        except IOError:
            print('File config.json has been not found .... ')
            print('Looking for file in folder:', self.configFilePath)
            quit()

        # check on serverPort key
        if 'serverPort' in data:
            self.setServerPort(data['serverPort'])
        else:
            print('Error: missing configuration key serverPort')
            quit()

        # check on serverIpAddress key
        if 'serverIpAddress' in data:
            self.setServerIpAddress(data['serverIpAddress'])
        else:
            print('Error: missing configuration key serverIpAddress')
            quit()

    # Getters
    def getServerPort(self) -> int:
        return int(self.serverPort)

    def getServerIpAddress(self) -> str:
        return str(self.serverIpAddress)

    # Setters
    def setServerPort(self, value: int) -> None:
        self.serverPort = value

    def setServerIpAddress(self, value: str) -> None:
        self.serverIpAddress = value

    def get_app_path(self) -> str:
        return _ROOT_FOLDER
